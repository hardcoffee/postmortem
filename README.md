## What's about?
** Post-mortem is a game made it for Global Game Jam (GGJ) 2018, who use the theme "Transmission". The entire game is autoral, like code, music and art. **

GGJ Site: https://globalgamejam.org/2018/games/post-mortem

Sinopse:
You was captured and tortured during the world war I. For some reason, you escaped! And with this freedom, you need to send a rescue message by morse code to your alies. After this, you need to go trought the scenes
searching clues who can be useful for your alies.

---
## Structure

Above, the scenes and screens who made the game:

1. Menu
2. Scene 1
3. Scene 2
4. Scene 3
---
## Next steps

What we'll do with the game?

1. Complete the basic cycle with the structures
2. Publish in website of Hardcoffee Game Studio
3. Publish in GameJolt plataform
4. Make a version to facebook page
---
## Sugestions?

Send an email to us: hardcoffeestudio@gmail.com