﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlotScript : MonoBehaviour {

    public int index = 0;
    private Image imagem_item;

    private void Awake()
    {
        imagem_item = GetComponent<Image>();
    }

    // Use this for initialization
    void Start()
    {
        if (PlayerPrefs.GetInt(index.ToString()) == 1) { imagem_item.enabled = true; } else { imagem_item.enabled = false; }
    }

    // Update is called once per frame
    public void atualiza_item()
    {
        if (PlayerPrefs.GetInt(index.ToString()) == 1) { imagem_item.enabled = true; } else { imagem_item.enabled = false; }
    }

}
