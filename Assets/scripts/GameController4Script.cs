﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class GameController4Script : MonoBehaviour {

    // Panels da maquina morse
    public GameObject panel_texto;
    public GameObject canvas_end;
    public Text texto_principal;
    public GameObject cadaver;
    public GameObject texto_final;
    public AudioClip musica_tema;

    // Fim de jogo
    public Text texto_fim;
    public Image logo;
    public Image Image_Next;
    public GameObject bg_cadaver;

    float timer = 10; // Contador de tempo crescente
    float timerTexto = 0; // Contador de tempo crescente
    bool isBlink = false; // Executar o pisca

    // Textos da tela
    private string[] textos = new string[3];
    public GameObject canvas_piscar;

    // Configurações de fade
    public float timer_cadaver = 1f;
    public float fade_timestamp = 0f;
    private int mostra_cadaver = 0;
    private int mostra_logo = 0;
    

    void Start ()
    {
        if (GameManagerScript.Instance != null) { 
            //GameManagerScript.Instance.PlayTrilhaSonora(musica_tema);
        }
        textos[0] = "I don’t believe... I’m in that room again!";
        textos[1] = "What is happening?!";
        textos[2] = "It...It can’t be!That’s...me ?!";

        texto_principal.text = textos[0];
        
        // Desabilita texto e imagem FIM
        logo.color = Color.Lerp(new Color(1, 1, 1, 0), Color.white, 0);
        texto_fim.color = Color.Lerp(new Color(1, 1, 1, 0), Color.white, 0);
    }
    private void Update()
    {
        if(timerTexto >= 6.5) { texto_principal.text = textos[2]; }
        else if (timerTexto >= 2) { texto_principal.text = textos[1]; }
        else if (timerTexto >= 0) { texto_principal.text = textos[0]; isBlink = true; }

        timerTexto += Time.deltaTime;
        if (timerTexto >= 14)
        {
            panel_texto.SetActive(false);
        }
        if (timerTexto >= 16)
        {
            canvas_end.SetActive(true);
            if (mostra_logo == 0)
            {
                StartCoroutine(FadeText(false, texto_final.GetComponent<Text>()));
                mostra_logo = 1;
                //StartCoroutine(EncerraMusica());
            }
        }

        // Se deve começar a executar o pisca
        if (isBlink)
        {
            if (timer <= 0.8) { canvas_piscar.SetActive(false); }
            else if (timer <= 1) { canvas_piscar.SetActive(true); }
            //else if (timer <= 2) { canvas_piscar.SetActive(false); }
            //else if (timer <= 5) { canvas_piscar.SetActive(true); }
            else if (timer <= 3.5) {
                canvas_piscar.SetActive(false);
                if (mostra_cadaver == 0)
                {
                    //StartCoroutine(FadeImage(false, cadaver.GetComponent<Image>()));
                    StartCoroutine(FadeSprite(false, bg_cadaver));
                    mostra_cadaver = 1;
                }
            }
            else if (timer <= 6) { canvas_piscar.SetActive(true); }
            else if (timer <= 8.8) { canvas_piscar.SetActive(false); }
            else if (timer <= 9) { canvas_piscar.SetActive(true); }
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                isBlink = false;
            }
        }

    }

    IEnumerator FadeImage(bool fadeAway, Image image_fade)
    {
        // fade from opaque to transparent
        if (fadeAway)
        {
            // loop over 1 second backwards
            for (float i = 1; i >= 0; i -= Time.deltaTime)
            {
                // set color with i as alpha
                //img.color = new Color(1, 1, 1, i);
                image_fade.GetComponent<Image>().color = new Color(1, 1, 1, i);
                yield return null;
            }
        }
        // fade from transparent to opaque
        else
        {
            // loop over 1 second
            for (float i = 0; i <= 1; i += Time.deltaTime)
            {
                // set color with i as alpha
                image_fade.GetComponent<Image>().color = new Color(1, 1, 1, i);
                yield return null;
            }
        }
    }
    IEnumerator FadeSprite(bool fadeAway, GameObject image_fade)
    {
        // fade from opaque to transparent
        if (fadeAway)
        {
            // loop over 1 second backwards
            for (float i = 1; i >= 0; i -= Time.deltaTime)
            {
                // set color with i as alpha
                //img.color = new Color(1, 1, 1, i);
                image_fade.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, i);
                yield return null;
            }
        }
        // fade from transparent to opaque
        else
        {
            // loop over 1 second
            for (float i = 0; i <= 1; i += Time.deltaTime)
            {
                // set color with i as alpha
                image_fade.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, i);
                yield return null;
            }
        }
    }
    IEnumerator FadeText(bool fadeAway, Text texto_fade)
    {
        // fade from opaque to transparent
        if (fadeAway)
        {
            // loop over 1 second backwards
            for (float i = 1; i >= 0; i -= Time.deltaTime)
            {
                // set color with i as alpha
                //img.color = new Color(1, 1, 1, i);
                texto_fade.GetComponent<Text>().color = new Color(1, 1, 1, i);
                yield return null;
            }
        }
        // fade from transparent to opaque
        else
        {
            Debug.Log(fadeAway);
            
            // loop over 1 second
            for (float i = 0; i <= 1; i += Time.deltaTime)
            {
                // set color with i as alpha
                texto_fade.GetComponent<Text>().color = new Color(1, 1, 1, i);
                yield return null;
            }
        }
    }

    public void finalizaJogo()
    {
        Image_Next.enabled = false;
        StartCoroutine(FadeText(true, texto_final.GetComponent<Text>()));
        StartCoroutine(FadeText(false, texto_fim));
        StartCoroutine(FadeImage(false, logo));
        StartCoroutine(MenuScene());
    }

    public void nextScene()
    {
        SceneManager.LoadScene("game_over");
    }

    IEnumerator MenuScene()
    {
        yield return new WaitForSeconds(5);
        float fadeTime = this.GetComponent<Fading>().BeginFade(5);
        StartCoroutine(loadNextScene(fadeTime, "scene_menu"));
    }

    IEnumerator loadNextScene(float secToWait, string scene)
    {
        yield return new WaitForSeconds(secToWait);
        SceneManager.LoadScene(scene);
    }

    IEnumerator EncerraMusica()
    {
        yield return new WaitForSeconds(10);
        if (GameManagerScript.Instance)
        {
            GameManagerScript.Instance.StopTrilhaSonora();
        }
    }


}
