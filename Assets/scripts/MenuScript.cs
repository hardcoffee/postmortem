﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour {

    public GameObject menu;
    public GameObject sobre;
    public AudioSource clicaNoBotao, passaOMouse;

    // Fade entre cenas
    public GameObject Fading;

    // Use this for initialization
    void Start () {
        // Zera os itens vitais no inicio do jogo
        PlayerPrefs.SetInt(0.ToString(), 0);
        PlayerPrefs.SetInt(1.ToString(), 0);
        PlayerPrefs.SetInt(2.ToString(), 0);
        // Marca se já passou por aquela cena
        PlayerPrefs.SetString("visitou_cena1", "0");
        PlayerPrefs.SetString("visitou_cena2", "0");
        PlayerPrefs.SetString("visitou_cena3", "0");

        if (GameManagerScript.Instance != null)
        {
            GameManagerScript.Instance.StopTrilhaSonora();
            GameManagerScript.Instance.trilha_tocando = 0;
            GameManagerScript.Instance.zeraJogo();
        }
    }

    public void Jogar()
    {
        float fadeTime = this.GetComponent<Fading>().BeginFade(1);
        StartCoroutine(loadNextScene(fadeTime));
    }
    IEnumerator loadNextScene(float secToWait)
    {
        yield return new WaitForSeconds(secToWait);
        SceneManager.LoadScene("scene_1");
    }

    public void Sair()
    {
        Application.Quit();
    }
    public void openSobre()
    {
        sobre.SetActive(true);
        menu.SetActive(false);
    }
    public void closeSobre()
    {
        sobre.SetActive(false);
        menu.SetActive(true);
    }

    // Evento de click no botão
    public void ClickButtonSound()
    {
        //clicaNoBotao.volume = 0.08f;
        clicaNoBotao.Play();
    }

    // Evento de click no botão
    public void HoverButtonSound()
    {
        passaOMouse.volume = 0.08f;
        passaOMouse.Play();
    }
}
