﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class GameController1Script : MonoBehaviour {

    // Estado de testes
    public GameObject papel_amassado;
    public GameObject cadeira;
    public GameObject som_eletrico;
    public GameObject som_ofegante;

    // Panels
    public GameObject panel_texto;
    public Text texto_principal;
    public GameObject panel_papel_item;
    public GameObject panel_chave_item;
    public GameObject panel_itens;
    public GameObject panel_box_itens;
    public Text panel_box_itens_text;
    public GameObject seta_cena;
    public Text painel_info_item_text;

    public Image slot_papel_amassado;
    public Button button_papel_amassado;
    public Button button_chave;
    public Image slot_chave;
    public AudioClip musica_tema;

    // Textos da tela
    private int cont_texto = 0;
    private string[] textos = new string[3];

    // Panels
    public GameObject panel_intro;
    public GameObject panel_hud;
   
    void Start ()
    {
        // Inicia ativando intro, desativando HUD e botões para começar o jogo
        if (GameManagerScript.Instance.intro_view == 0)
        {
            panel_intro.SetActive(true);
            panel_hud.SetActive(false);
            button_papel_amassado.enabled = false;
            button_chave.enabled = false;
            GameManagerScript.Instance.intro_view = 1;
        }
        // Se o papel já tiver sido clicado antes, libera o panel
        if (GameManagerScript.Instance.paper_view == 1) { button_papel_amassado.enabled = true; }
        // Se a chave já tiver sido clicada antes, libera o panel
        if (GameManagerScript.Instance.chave_view == 1) { button_chave.enabled = true; }

        Debug.Log(GameManagerScript.Instance.trilha_tocando);
        if (GameManagerScript.Instance != null)
        {
            GameManagerScript.Instance.PlayTrilhaSonora(musica_tema);
            GameManagerScript.Instance.trilha_tocando = 1;
        }

        textos[0] = "Where is everyone? I don’t like what is going on here...";
        textos[1] = "I don’t feel well. I need to get out from here. I must find a way to contact my crew.";
        //textos[2] = "Não me sinto bem, preciso sair daqui logo. Vou encontrar um meio de pedir resgate aos meus camaradas.";
        texto_principal.text = textos[0];

        // Se já visitou antes, nao mostra mais o texto, desabilita || Desabilita o panel de texto também
		if (PlayerPrefs.GetString ("visitou_cena1") == "1") {
			panel_texto.SetActive (false);
			som_eletrico.SetActive (false);
			som_ofegante.SetActive (false);
		} else { // Se não visitou a cena 1 ainda
            // Inicialmente, seta os itens e a seta de cena desativados para que o jogador leia o texto antes de tudo
            //panel_itens.SetActive(false); Antes sumia tudo, agora só desativa
            papel_amassado.GetComponent<Button>().enabled = false;
            cadeira.SetActive(false);
            seta_cena.SetActive(false);
		}
		// Se papel ou chave pegos
        if (PlayerPrefs.GetInt(0.ToString()) == 1) {
            slot_papel_amassado.enabled = true;
            papel_amassado.SetActive(false);
            button_papel_amassado.enabled = true;
        }
        if (PlayerPrefs.GetInt(1.ToString()) == 1) {
            slot_chave.enabled = true;
            button_chave.enabled = true;
        }

        // Marca que ja passou por aqui
        PlayerPrefs.SetString("visitou_cena1", "1");
    }

    public void cadeira_clicada()
    {
        panel_box_itens.SetActive(false);
        panel_box_itens_text.text = "I lived a hell in that chair...I must get out here.";
        panel_box_itens.SetActive(true);
        button_papel_amassado.enabled = false;
        button_chave.enabled = false;
    }

    public void nextScene()
    {
        float fadeTime = this.GetComponent<Fading>().BeginFade(1);
        StartCoroutine(loadNextScene(fadeTime));
    }
    IEnumerator loadNextScene(float secToWait)
    {
        yield return new WaitForSeconds(secToWait);
        SceneManager.LoadScene("scene_2");
    }

    public void papel_clicado () {
        panel_itens.SetActive(false);
        //painel_info_item_text.text = "Encontrei uma pedaço de coordenada!";
        PlayerPrefs.SetInt(0.ToString(), 1);
        slot_papel_amassado.enabled = true;
        panel_papel_item.SetActive(true);
        papel_amassado.SetActive(false);
        // Marca que o botão pode ser visto para abrir em outras cenas
        button_papel_amassado.enabled = true;
        button_chave.enabled = false;
        GameManagerScript.Instance.paper_view = 1;
    }
    public void chave_clicado()
    {
        panel_itens.SetActive(false);
        panel_chave_item.SetActive(true);
        button_papel_amassado.enabled = false;
        button_chave.enabled = false;
    }

    public void nextText()
    {
        cont_texto++;
        if (cont_texto < 2) { 
            texto_principal.text = textos[cont_texto];
        } else {
            panel_texto.SetActive(false);
			// Ativa os itens e seta para trocar de cena
			//panel_itens.SetActive(true);
            papel_amassado.GetComponent<Button>().enabled = true;
            cadeira.SetActive(true);
            seta_cena.SetActive(true);
        }
    }

    public void fecharPanelItem()
    {
        panel_box_itens.SetActive(false);
        panel_papel_item.SetActive(false);
        panel_chave_item.SetActive(false);
        panel_itens.SetActive(true);

        // Se o papel já tiver sido clicado antes, libera o panel
        if (GameManagerScript.Instance.paper_view == 1) { button_papel_amassado.enabled = true; }
        // Se a chave já tiver sido clicada antes, libera o panel
        if (GameManagerScript.Instance.chave_view == 1) { button_chave.enabled = true; }
    }

    public void fecharPanelIntro()
    {
        panel_intro.SetActive(false);
        panel_hud.SetActive(true);
    }
}
