﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InputMorseScript : MonoBehaviour {

    public string codigo_sucesso;
    public InputField codigo_morse;
    public Button button_ponto;
    public Button button_traco;
    private int contador_char = 0;
    float timer = 4; // Contador de tempo decrescente pisca
    float timerRussian = 0; // Contador de tempo crescente audio russo
    float timerAbajour = 20; // Contador de tempo crescente pisca abajour
    bool isBlink = false; // Executar o pisca
    bool isRussian = false; // Executar o audio russo
    bool isAbajour = false; // Piscar abajour
    bool isAbajourPlayed = false; // Piscar abajour

    // Panels da maquina morse
    public GameObject canvas_morse;
    public GameObject canvas_piscar;
    public GameObject panel_maquina;
    public GameObject som_acerto;
    public GameObject som_erro;
    public GameObject som_russos;
    public AudioClip som_final;
    // Área de texto
    public GameObject panel_text;
    public Text panel_textext;
    public GameObject pass_text;
    //legenda texto russo
    private string[] lengenda_russo = new string[6];

    void Start ()
    {
        // Texto de legenda russa
        lengenda_russo[0] = "Base, can you hear me? Over.";
        lengenda_russo[1] = "Proceed, captain. Over.";
        lengenda_russo[2] = "We found the body of Soldier Ivan. He was in a bunker to the north.";
        lengenda_russo[3] = "Clear signals of torture. Torn apart body. Over.";
        lengenda_russo[4] = "Captain, bring the body to the base and collect info about that bunker. Over.";
        lengenda_russo[5] = "Understood, base. Over and out.";
    }
	void Update () {
        // Se deve começar a tocar o audio russo
        if (isRussian)
        {
            if (timerRussian >= 21) { panel_textext.text = lengenda_russo[5]; }
            else if (timerRussian >= 15) { panel_textext.text = lengenda_russo[4]; }
            else if (timerRussian >= 12) { panel_textext.text = lengenda_russo[3]; }
            else if (timerRussian >= 7) { panel_textext.text = lengenda_russo[2]; }
            else if (timerRussian >= 4) { panel_textext.text = lengenda_russo[1]; }
            else if (timerRussian >= 0) { panel_textext.text = lengenda_russo[0]; }
            
            timerRussian += Time.deltaTime;
            if (timerRussian >= 25)
            {
                panel_text.SetActive(false);
                isRussian = false;
                isBlink = true;
            }
            if (!isAbajour)
            {
                isAbajour = true;
            }
        }
        // Se deve começar a executar o pisca
        if (isBlink)
        {
            if (timer <= 0.8) { canvas_piscar.SetActive(false); }
            else if (timer <= 1) { canvas_piscar.SetActive(true); }
            else if (timer <= 1.8) { canvas_piscar.SetActive(false); }
            else if (timer <= 2) { canvas_piscar.SetActive(true); }
            else if (timer <= 2.8) { canvas_piscar.SetActive(false); }
            else if (timer <= 3) { canvas_piscar.SetActive(true); }
            else if (timer <= 3.2) { canvas_piscar.SetActive(false); }
            else if (timer <= 3.8) { canvas_piscar.SetActive(true); }
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                isBlink = false;
                //StartCoroutine(espera_segundos());
                SceneManager.LoadScene("scene_4");
            }
        }
        // Se deve começar a piscar o abajour
        if (isAbajour)
        {
            if (timerAbajour <= 17 && !isAbajourPlayed) {
                this.GetComponent<GameController3Script>().luminaria_clicado();
                isAbajourPlayed = true;
            }
            if (timerAbajour <= 15 && isAbajourPlayed) {
                this.GetComponent<GameController3Script>().luminaria_clicado();
                isAbajourPlayed = false;
            }
            if (timerAbajour <= 14 && !isAbajourPlayed)
            {
                this.GetComponent<GameController3Script>().luminaria_clicado();
                isAbajourPlayed = true;
            }
            if (timerAbajour <= 10 && isAbajourPlayed)
            {
                this.GetComponent<GameController3Script>().luminaria_clicado();
                isAbajourPlayed = false;
            }
            if (timerAbajour <= 8 && !isAbajourPlayed)
            {
                this.GetComponent<GameController3Script>().luminaria_clicado();
                isAbajourPlayed = true;
            }

            timerAbajour -= Time.deltaTime;
            if (timerAbajour <= 0)
            {
                isAbajour = false;
            }
        }
    }

    public void AddCodeMorsePonto ()
    {
        codigo_morse.text += ".";
        contador_char++;
        verifica_mensagem(codigo_morse.text);
    }
    public void AddCodeMorseTraco ()
    {
        codigo_morse.text += "-";
        contador_char++;
        verifica_mensagem(codigo_morse.text);
    }
    public void RemoveCodeMorse ()
    {
        string gambiarra = "";
        for (int i = 0; i < codigo_morse.text.Length-1; i++)
        {
            gambiarra += codigo_morse.text[i];
        }
        codigo_morse.text = gambiarra;
        contador_char--;
    }

    public void playLastTheme()
    {
        if (GameManagerScript.Instance)
        {
            GameManagerScript.Instance.PlayTrilhaSonora(som_final);
        }
    }

    public void verifica_mensagem(string codigo_morse_enviado){
        
        if (codigo_morse_enviado == codigo_sucesso)
        {
            panel_maquina.SetActive(false);
            som_acerto.GetComponent<AudioSource>().Play();
            canvas_morse.SetActive(false);
            //StartCoroutine(espera_segundos()); Antes mandava ir direto pra cena seguinte, agora manda tocar russo

            // Prepara a caixa de texto, retirando a interação do usuário e ativando ela
            pass_text.SetActive(false);
            panel_text.SetActive(true);
            som_russos.GetComponent<AudioSource>().Play();
            isRussian = true;
            this.GetComponent<GameController3Script>().stopMusicaMorse();
            StartCoroutine(espera_segundos_tema_final());
        }
        if (contador_char >= 21)
        {
            codigo_morse.text = "";
            som_erro.GetComponent<AudioSource>().Play();
            contador_char = 0;
        }
    }
    IEnumerator espera_segundos()
    {
        yield return new WaitForSeconds(0f);
        SceneManager.LoadScene("scene_4");
    }
    IEnumerator espera_segundos_tema_final()
    {
        yield return new WaitForSeconds(0f);
        playLastTheme();
    }
}
