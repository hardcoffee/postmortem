﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lampadaScript : MonoBehaviour {

    public bool lampada_ligada = false;
    private int position = 0;
    private float[] tempos = new float[5];

    public float countdown = 3.0f;

    void Start ()
    {
        tempos[0] = 3f;
        tempos[1] = 1f;
        tempos[2] = 3f;
        tempos[3] = 3f;
        tempos[4] = 5f;
    }
	
	void Update ()
    {
        countdown -= Time.deltaTime;

        if (countdown <= 0) {
            if (!lampada_ligada)
            {
                this.GetComponent<Light>().enabled = true; position++; lampada_ligada = true;
            } else { this.GetComponent<Light>().enabled = false; position++; lampada_ligada = false; }
            countdown = tempos[position%5];
        }

    }


}
