﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class GameController3Script : MonoBehaviour {

    // Panels da maquina morse
    public GameObject panel_texto;
    public Text texto_principal;
    public GameObject panel_itens;
    public GameObject panel_Box_item;
    public GameObject canvas_inventario_papel;
    public Image imagem_inventario_papel;
    public Text canvas_inventario_papel_text;
    public GameObject canvas_inventario_chave;
    public Text painel_info_item_text;
    public GameObject inventario;

    public GameObject bilhete;
    public GameObject luz_luminaria;
    public GameObject luz_radio;
    public GameObject radio;

    public GameObject maquina_morse;

    public GameObject musica_morse;
    public GameObject som_lumi_ligando;
    public GameObject som_lumi_desligando;
    public GameObject som_radio;
    public AudioClip musica_tema;
    public AudioClip som_final;

    public Sprite papel_amassado_completo;
    public Image slot_papel_amassado;
    public Image slot_chave;
    public Button button_papel;
    public Button button_chave;

    public GameObject camera_principal;

    // Textos da tela
    private int cont_texto = 0;
    private string[] textos = new string[3];

    void Start ()
    {
        // Texto de intro da cena
        textos[0] = "Damn! This door is stuck. So, it's better left behind what happened in that room.";
        textos[1] = "I must find out where am I and ask for help.";
        texto_principal.text = textos[0];
        // Marca que ja passou por aqui
        PlayerPrefs.SetString("visitou_cena3", "1");
        
        // Assim que entra na cena, preparar o ambiente
        panel_itens.SetActive(false);
        button_papel.enabled = false;
        button_chave.enabled = false;
        slot_papel_amassado.enabled = true;
        slot_chave.enabled = true;
    }
	
    public void nextScene()
    {
        SceneManager.LoadScene("scene_4");
    }
    public void bilhete_clicado() {
        panel_itens.SetActive(false);
        panel_Box_item.SetActive(true);
        painel_info_item_text.text = "UVB-76! That’s the full coordinate that I found a piece before. I’ll ask for a rescue using this radio.";
        PlayerPrefs.SetInt(2.ToString(), 1);
        if (GameManagerScript.Instance) { GameManagerScript.Instance.bilhete_clicked = 1; }
        imagem_inventario_papel.sprite = papel_amassado_completo;
        canvas_inventario_papel_text.text = "Coordenada Completa";
    }
    public void painel_clicado() {
        panel_itens.SetActive(false);
        painel_info_item_text.text = "I would not put my hand in there...";
        panel_Box_item.SetActive(true);
    }
    public void painel_2_clicado()
    {
        panel_itens.SetActive(false);
        painel_info_item_text.text = "Just an electrical panel...";
        panel_Box_item.SetActive(true);
    }
    public void porta_clicado() { panel_itens.SetActive(false); painel_info_item_text.text = "It’s useful. It’s jammed."; panel_Box_item.SetActive(true); }
    public void luminaria_clicado() {
        luz_luminaria.GetComponent<Light>().enabled = !luz_luminaria.GetComponent<Light>().enabled;
        if (luz_luminaria.GetComponent<Light>().enabled) {
            som_lumi_desligando.GetComponent<AudioSource>().Play();
        } else { som_lumi_ligando.GetComponent<AudioSource>().Play(); }
    }

    public void radio_clicado()
    {
        luz_radio.GetComponent<Light>().enabled = !luz_radio.GetComponent<Light>().enabled;
        if (luz_radio.GetComponent<Light>().enabled)
        {
            som_radio.GetComponent<AudioSource>().Play();
        }
        else { som_radio.GetComponent<AudioSource>().Stop(); }
    }
    public void radio_velho_clicado() {
        if (GameManagerScript.Instance)
        {
            if (GameManagerScript.Instance.bilhete_clicked == 0)
            {
                panel_itens.SetActive(false);
                painel_info_item_text.text = "Looks like this radio is not used for a while...";
                panel_Box_item.SetActive(true);
            }
            else
            {
                // Inicia tela de morse
                if (GameManagerScript.Instance) { GameManagerScript.Instance.StopTrilhaSonora(); }
                musica_morse.GetComponent<AudioSource>().Play();
                panel_itens.SetActive(false);
                // Desativa o scroll da camera e o inventario
                camera_principal.GetComponent<followMouseScript>().enabled = false;
                inventario.SetActive(false);
                // Chama a tela de morse
                maquina_morse.SetActive(true);
            }
        } else {
            panel_itens.SetActive(false);
            painel_info_item_text.text = "Looks like this radio is not used for a while...";
            panel_Box_item.SetActive(true);
        }
    }
    public void quadro_clicado() { panel_itens.SetActive(false); painel_info_item_text.text = "A picture of someone important."; panel_Box_item.SetActive(true); }
    public void relatorio_1_clicado() {
        panel_itens.SetActive(false);
        painel_info_item_text.text = "It seems that this is a radio shortwave station which transmits a frequency 4625 kHz.";
        panel_Box_item.SetActive(true);
    }
    public void relatorio_2_clicado() {
        panel_itens.SetActive(false);
        painel_info_item_text.text = "Just a radio frequency list.";
        panel_Box_item.SetActive(true);
    }

    public void papel_clicado()
    {
        panel_itens.SetActive(false);
        canvas_inventario_chave.SetActive(false);
        canvas_inventario_papel.SetActive(true);
        button_papel.enabled = false;
        button_chave.enabled = false;
    }
    public void chave_clicado()
    {
        panel_itens.SetActive(false);
        canvas_inventario_chave.SetActive(true);
        canvas_inventario_papel.SetActive(false);
        button_papel.enabled = false;
        button_chave.enabled = false;
    }

    public void stopMusicaMorse()
    {
        musica_morse.GetComponent<AudioSource>().Stop();
    }

    public void nextText()
    {
        cont_texto++;
        if (cont_texto < 2) { 
            texto_principal.text = textos[cont_texto];
        } else {
            panel_texto.SetActive(false);
            panel_itens.SetActive(true);
            button_papel.enabled = true;
            button_chave.enabled = true;
        }
    }

    public void fecharPanelItem() {
        panel_Box_item.SetActive(false);
        panel_itens.SetActive(true);
        canvas_inventario_papel.SetActive(false);
        canvas_inventario_chave.SetActive(false);
        button_papel.enabled = true;
        button_chave.enabled = true;
    }

    public void fecharPanelMorse()
    {
        panel_Box_item.SetActive(false);
        panel_itens.SetActive(true);
        canvas_inventario_papel.SetActive(false);
        canvas_inventario_chave.SetActive(false);
        button_papel.enabled = true;
        button_chave.enabled = true;

        musica_morse.GetComponent<AudioSource>().Stop();
        if (GameManagerScript.Instance) { GameManagerScript.Instance.PlayTrilhaSonora(musica_tema); }
        camera_principal.GetComponent<followMouseScript>().enabled = true;
        inventario.SetActive(true);
        maquina_morse.SetActive(false);
    }
}
