﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class luminariaScript : MonoBehaviour {

    public float smooth = 2.0F;
    public float tiltAngle = 2.5F;
    public float angle = 25.0F;
    public int directionAngle = 1;
    public bool sentido = false;

    void Start ()
    {
    }
	
	void Update () {

        if (sentido) { angle++; if (angle >= 25) { sentido = false; } }
        else { angle--; if (angle <= -25) { sentido = true; } }
        if (angle >= 0) { directionAngle = 1; } else { directionAngle = -1; }

        transform.RotateAround(transform.position, new Vector3(0,0,1), directionAngle * tiltAngle * Time.deltaTime * smooth);
    }


    IEnumerator espera_segundos()
    {
        yield return new WaitForSeconds(2f);
    }
}
