﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class GameController2Script : MonoBehaviour {

    // Panels da maquina morse
    public GameObject panel_texto;
    public GameObject panel_box_item;
    public GameObject panel_itens;
    public GameObject canvas_inventario;
    public GameObject canvas_inventario_chave;
    public Text canvas_chave_text;
    public GameObject chave;
    public Text painel_info_item_text;
    // Som
    public GameObject som_porta_trancada;
    public GameObject som_porta_abrindo;
    public GameObject som_porta_destrancada_abrindo;
    public GameObject som_choque_eletrico;
    // Slot
    public Image slot_papel_amassado;
    public Button button_papel;
    public Button button_chave;
    public Image slot_chave;
    public Image Next_Scene;

    void Start ()
    {

        // Se já visitou antes, nao mostra mais o texto, desabilita o panel de texto
        if (PlayerPrefs.GetString("visitou_cena2") == "1")
        {
            panel_texto.SetActive(false);
            button_papel.enabled = false;
            button_chave.enabled = false;
        }

        // Se tiver achado o papel antes, ativa o botao
        if (GameManagerScript.Instance) { 
            if (GameManagerScript.Instance.paper_view == 1)
            {
                button_papel.enabled = true;
            }
            // Se tiver achado a chave antes, ativa o botao
            if (GameManagerScript.Instance.chave_view == 1)
            {
                button_chave.enabled = true;
            }
        }
        if (PlayerPrefs.GetInt(0.ToString()) == 1)
        {
            slot_papel_amassado.enabled = true;
        }
        if (PlayerPrefs.GetInt(1.ToString()) == 1)
        {
            slot_chave.enabled = true;
            chave.SetActive(false);
        }

        // Marca que ja passou por aqui
        PlayerPrefs.SetString("visitou_cena2", "1");
    }
    private float timerTexto = 0;
    private void Update()
    {
        timerTexto += Time.deltaTime;
        if (timerTexto >= 9)
        {
            timerTexto = 0;
            som_choque_eletrico.GetComponent<AudioSource>().Play();
        }
    }

    public void chave_clicado()
    {
        // Se papel e chave encontrados
        if (PlayerPrefs.GetInt(0.ToString()) == 1 && PlayerPrefs.GetInt(1.ToString()) == 1) { Next_Scene.enabled = false; }
        if (GameManagerScript.Instance)
        {
            if (GameManagerScript.Instance.chave_view == 1) { canvas_chave_text.text = "Chave"; }
            GameManagerScript.Instance.chave_view = 1;
        }
        panel_itens.SetActive(false);
        chave.SetActive(false);
        PlayerPrefs.SetInt(1.ToString(), 1);
        slot_chave.enabled = true;
        canvas_inventario_chave.SetActive(true);
        button_papel.enabled = false;
        button_chave.enabled = false;
    }
    public void remedios_clicado() {
        panel_itens.SetActive(false);
        painel_info_item_text.text = "A first aid cabinet without anything that can be useful to me.";
        panel_box_item.SetActive(true);
        button_papel.enabled = false;
        button_chave.enabled = false;
    }
    public void ferramentas_clicado() {
        panel_itens.SetActive(false);
        painel_info_item_text.text = "A toolbox full of useless rotten tools. Can’t find anything helpful.";
        panel_box_item.SetActive(true);
        button_papel.enabled = false;
        button_chave.enabled = false;
    }
    public void livros_clicado() {
        panel_itens.SetActive(false);
        painel_info_item_text.text = "Hmm. Some radio operational manuals. There is any station here?";
        panel_box_item.SetActive(true);
        button_papel.enabled = false;
        button_chave.enabled = false;
    }
    public void latas_clicado() {
        panel_itens.SetActive(false);
        painel_info_item_text.text = "I’m hungry but there is nothing that I can eat in those cans.";
        panel_box_item.SetActive(true);
        button_papel.enabled = false;
        button_chave.enabled = false;
    }
    public void caixa_papelao_clicado() {
        panel_itens.SetActive(false);
        painel_info_item_text.text = "That place is abandoned… These boxes are empty.";
        panel_box_item.SetActive(true);
        button_papel.enabled = false;
        button_chave.enabled = false;
    }
    public void papel_clicado() {
        panel_itens.SetActive(false);
        canvas_inventario.SetActive(true);
        button_papel.enabled = false;
        button_chave.enabled = false;
    }
    public void fechar_papel_clicado() {
        canvas_inventario.SetActive(false);
        panel_itens.SetActive(true);
        // Se o papel já tiver sido clicado antes, libera o panel
        if (GameManagerScript.Instance.paper_view == 1) { button_papel.enabled = true; }
        // Se a chave já tiver sido clicada antes, libera o panel
        if (GameManagerScript.Instance.chave_view == 1) { button_chave.enabled = true; }
    }
    public void fechar_chave_clicada() {
        canvas_inventario_chave.SetActive(false);
        panel_itens.SetActive(true);
        // Se o papel já tiver sido clicado antes, libera o panel
        if (GameManagerScript.Instance.paper_view == 1) { button_papel.enabled = true; }
        // Se a chave já tiver sido clicada antes, libera o panel
        if (GameManagerScript.Instance.chave_view == 1) { button_chave.enabled = true; }
    }

    public void BackScene()
    {
        float fadeTime = this.GetComponent<Fading>().BeginFade(1);
        StartCoroutine(loadNextScene(fadeTime, "scene_1"));
    }
    // Clique no trinco
    public void NextScene()
    {
        if ((PlayerPrefs.GetInt(1.ToString()) == 1) && (PlayerPrefs.GetInt(0.ToString()) == 1)) {
            som_porta_abrindo.GetComponent<AudioSource>().Play();
            som_porta_destrancada_abrindo.GetComponent<AudioSource>().Play();
            float fadeTime = this.GetComponent<Fading>().BeginFade(1);
            StartCoroutine(loadNextScene(fadeTime, "scene_3"));
        }
        else if ((PlayerPrefs.GetInt(0.ToString()) == 0) && (PlayerPrefs.GetInt(1.ToString()) == 1))
        {
            painel_info_item_text.text = "This key is to this door but I feel that is something more to find out from this room.";
            panel_box_item.SetActive(true);
            som_porta_trancada.GetComponent<AudioSource>().Play();
        } else {
            panel_itens.SetActive(false);
            painel_info_item_text.text = "Locked… I must find a way to open this door.";
            panel_box_item.SetActive(true);
            som_porta_trancada.GetComponent<AudioSource>().Play();
        }
    }
    IEnumerator loadNextScene(float secToWait, string scene)
    {
        yield return new WaitForSeconds(secToWait);
        SceneManager.LoadScene(scene);
    }

    public void fecharPanelItem() {
        panel_box_item.SetActive(false);
        panel_itens.SetActive(true);
        // Se o papel já tiver sido clicado antes, libera o panel
        if (GameManagerScript.Instance.paper_view == 1) { button_papel.enabled = true; }
        // Se a chave já tiver sido clicada antes, libera o panel
        if (GameManagerScript.Instance.chave_view == 1) { button_chave.enabled = true; }
    }
}
