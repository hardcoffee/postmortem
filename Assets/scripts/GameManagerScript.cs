﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerScript : MonoBehaviour {

    // SOM
    public AudioSource trilha_sonora;
    // VARIAVEIS
    public int trilha_tocando = 1;
    public int intro_view = 0;
    public int paper_view = 0;
    public int chave_view = 0;
    public int bilhete_clicked = 0;

    public static GameManagerScript Instance { get; private set; }

    // Primeira instancia geral, não permitindo outras instancias
    private void Awake () {
        if (Instance == null) {
            Instance = this;
        } else if (Instance != this) {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }
    public void zeraJogo()
    {
        trilha_tocando = 1;
        intro_view = 0;
        paper_view = 0;
        chave_view = 0;
        bilhete_clicked = 0;
    }
    public void PlayTrilhaSonora(AudioClip trilha) {
        trilha_sonora.clip = trilha;
        trilha_sonora.Play();
    }
    public void StopTrilhaSonora()
    {
        trilha_sonora.Stop();
    }


}
