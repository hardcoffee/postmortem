﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemMouseScript : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private Sprite actualImage;

    public Sprite imageEffect;

    public Texture2D cursorTexture;
    public CursorMode cursorMode = CursorMode.Auto;

    void Start()
    {
        actualImage = this.gameObject.GetComponent<Image>().sprite;
    }
    public void objeto_clicado()
    {
        this.gameObject.GetComponent<Image>().sprite = actualImage;
        Cursor.SetCursor(null, Vector2.zero, cursorMode);
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        this.gameObject.GetComponent<Image>().sprite = imageEffect; // Muda o efeito de objeto
        Cursor.SetCursor(cursorTexture, Vector2.zero, cursorMode); // Muda o cursor do mouse
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        this.gameObject.GetComponent<Image>().sprite = actualImage;
        Cursor.SetCursor(null, Vector2.zero, cursorMode);
    }

    public void setMouseDefault()
    {
        Cursor.SetCursor(null, Vector2.zero, cursorMode);
    }

}
